\documentclass[a4paper]{article}

\usepackage[margin=2cm]{geometry}
\usepackage{verbatim}
\usepackage{soul}
\usepackage{xcolor}
\usepackage{booktabs}
\usepackage{placeins}

\definecolor{monrouge}{RGB}{230,39,79}
\definecolor{monrose}{RGB}{171,39,79}
\definecolor{monvert}{RGB}{0,106,78}
\definecolor{monbleu}{RGB}{46,88,148}

\newcommand{\qu}[1]{\vspace{\baselineskip}{\textcolor{monbleu}{#1}}}
\newcommand{\re}[1]{\vspace{\baselineskip}{\it \noindent#1}~\\}
\newcommand{\tw}[1]{\vspace{\baselineskip}\noindent\textcolor{monrouge}{\bf #1}~\\}
\newcommand{\ci}[2]{$\triangleright$~{L. #1}: {\textcolor{monvert}{#2}}\\}

\newcommand{\TBD}{\textcolor{monrouge}{\bf !!TBD!!}}

\usepackage{graphicx}
\graphicspath{{figures/}}

\title{}
\author{Najda Villefranque, Howard Barker, Jason Cole and Zhipeng Qu}
\date{\today}

\begin{document}
\maketitle

% \qu{}
% \re{}
% \ci{}{}

\section*{Response to Editor}

We are very grateful that you accepted to edit our manuscript. Thank you for
your comments on the text and the reviewer's suggestions. In the following,
your comments and questions are reported in \textcolor{monbleu}{blue}, our
answers are in black italics and citations from the text in
\textcolor{monvert}{green}. Line numbers correspond to the new manuscript.

\qu{Both reviewers are positive about the attributes of this paper. One has
flagged it as requiring minor amendments while the other requests major, albeit
mostly textual, amendments. My judgement is that it is on the boundary between
major and minor.}

\qu{It is important that the clarifications requested by the reviewers are
responded to, as both reviewers indicate aspects of the methodology, and its
application.  that are unclear. As indicated by Reviewer 1, I felt that some
additional discussion is needed on the application to instances with
significant spectral variation in surface albedo, which is the case of over
vegetated and snow covered surfaces. As far as I could see, albedo was
specified as a single broadband value.}

\re{Thank you for your suggestion. Indeed the method is not limited to
broadband or homogeneous values, these were only used in the illustrations for
simplicity. A paragraph was added in Sec. 4 to explain how to use the FMC
method without these assumptions.}

\ci{}{The fact that a very limited amount of data needs to be recorded and
output was here due to the assumption that $\alpha$ was spectrally and
spatially constant. Should this be no longer the case, then additional
information would be needed to reconstruct the functional. The most
straightforward way would be to store for each path: the location of each
surface reflection, and the wavelength associated with the path. Then, given a
field of spectrally and spatially varying surface albedo, the sampled path
weigths would be corrected according to the detailed path--surface
interactions, preserving the correlations between the spatial and spectral
variations of the atmosphere and those of the surface.}

\newpage \section*{Response to Reviewer 1}

Thank you for your work, comments and insights. In addition to many changes in
the manuscript that we hope has improved its quality, your suggestions also
prompted us to perform more simulations leading to interesting results. More
thorough analysis will be needed to publish these results so we decided not to
include them in this paper, and we hope you will be satisfied with our
responses to your review. In the following, the overview, comments and
grammatical corrections from your review are reported in
\textcolor{monbleu}{blue}, our answers are in black italics and citations from
the text in \textcolor{monvert}{green}. Line numbers correspond to the new
manuscript.

\qu{The authors apply a Monte Carlo technique, which they termed
``functionalized Monte Carlo", to study the 3D radiative effects of clouds as
function of surface reflectivity. While the paper is generally well-written and
interesting for publication, I do have a couple of comments that should be
addressed:}

\qu{1. Ln 52-54 \& ln 435-437. Unless operational strictly refers to
operational weather forecasting, see Veerman et al. (2022;
https://doi.org/10.1029/2022GL100808) for coupled Monte Carlo radiative
transfer in large-eddy simulations.}

\re{The word ``operational" indeed refers to operational NWP and remote sensing
in this sentence. The suggested reference was added in the next sentence.}

\ci{}{They have, however, been used often to study complex interactions between
3D clouds and radiation --- including recently in a coupled high-resolution
simulation, see Veerman et al. (2022) --- and consequently to evaluate and
improve fast approximate RT models}

\qu{2. Ln 158. ``standard 3D MC RT model`: please provide more detail. What
model has been used exactly, how are (cloud droplet) scattering and absorption
handled, how do you handle spectral integration?}

\re{As suggested, more details were added in an Appendix to describe the 3D RT
model.}

\ci{}{The atmosphere is constituted of 601 plane-parallel layers of
horizontally homogeneous gas, whose optical properties are described using a
k-distribution model (Iacono et al., 2008), and of a 3D field of liquid cloud
water content with a uniform effective radius of 10 $\mu$m and whose optical
properties are described using a Mie model (Mishchenko et al., 2002). For each
path, a spectral band is sampled using the cumulative distribution of incoming
solar radiation at TOA, then a quadrature point is sampled according to the
k-distribution model, from which the gas absorption profile is set, and finally
a wavelength is sampled uniformly in the selected band, from which the cloud
optical properties are set. Then a photon's path is traced through the
atmosphere, beginning at TOA with a weight of $w$, until it either escapes to
space or gets absorbed in the atmosphere-surface system. The tracking is based
on the null-collision (maximum cross-section) method (Galtier et al., 2013;
Marchuk et al., 1980).  The Mie phase function is used to sample the scattered
direction. The simulation proceeds using a specific value of surface albedo,
$\hat{\alpha} $; when a path reaches the surface it is reflected (with
probability $\hat{\alpha}$) or absorbed (with probability $1-\hat{\alpha}$).}

\qu{3. Ln 160. Related to the spectral integration, is A a broadband flux or
defined per spectral interval/g-point? I assumed the albedo is spectrally
constant (but please state so explicitly), but the scattering from clouds and
gases is certainly not.}

\re{$A$ is a generic notation for a flux (could also be a radiance) so it could
be broadband or spectrally resolved; in our simulations it is always broadband.
A sentence was added to clarify this point.} 

\ci{}{More specifically, this FMC computes a solar flux, $A_{|\alpha}$, as a function
of albedo $\alpha$ of a planar, uniform, Lambertian surface. In the following
illustrations $A_{|\alpha}$ will be a broadband flux and $\alpha$ will be
spectrally constant. Note that this is not a fundamental assumption for the
method, as will be discussed in Section 4.}

\qu{4. Figure 2. The legends of the top right and bottom right subfigures are
not consistent (lines in top right, filled rectangles in bottom right).}

\re{Done.}

\qu{5. Figure 3. I am not sure I understand the relative uncertainty. Given a
downwelling surface flux of 800 W/m2, does a .25 uncertainty mean that the
standard deviation is 200 W/m2? That seems much higher than the uncertainty in
Figure 2 (left column).}

\re{This is because the number on the contour plot of Fig 3 is in percent so
the .25 uncertainty for 800 W/m$^2$ means $\pm$ 2 W/m$^2$ uncertainty. Sorry
for the confusion -- the ``$\%$" unit was added to the contour labels on the
plot and also in the caption.}

\qu{6. Ln 273-285. I suggest to move this paragraph to the introduction
section.}

\re{We followed this suggestion and inserted the paragraph before the
announcement of the structure of the paper in the introduction section.}

\qu{7. Figure 4. There is no in-text reference to this figure.}

\re{Thank you, a reference to Fig. 4 was added L\TBD.}

\qu{8. Ln 290-291: Are the results sensitive to cloud characteristics such as
cloud cover, liquid water path, cloud base/top height? I would recommend to use
multiple cloud fields to show how the results of e.g. figure 5 differ between
cloud fields. Alternatively, please comment on the expected sensitivity in the
discussion.}

\re{This is a very interesting question that deserves a full investigation. We
chose to add a paragraph to discuss this in Sec. 4 rather than extending the
study to new cloud fields. We still performed calculations in a stratocumulus
field (see the results in Fig.~\ref{fig:stcu} of this document), but chose not
to include the results in the paper to keep it short and focused.}
\begin{figure} \centering
  \includegraphics[height=.25\textheight]{fig_optdepth_map_SANDU.pdf}~
  \includegraphics[height=.25\textheight]{fig_rc_profiles_SANDU.pdf}~\\
  \includegraphics[width=.5\textwidth]{fig_xalb_y3Deffsurf_csza_SANDU.pdf}~
  \includegraphics[width=.5\textwidth]{fig_xalb_y3Defftoa_csza_SANDU.pdf}~\\
  \includegraphics[width=.5\textwidth]{fig_xnbref_y3Deffsurf_csza_SANDU.pdf}~
  \includegraphics[width=.5\textwidth]{fig_xnbref_y3Defftoa_csza_SANDU.pdf}~\\
  \caption{Same as Figure 4, Figure 5 and top 2 plots of Figure 6 of the
  manuscript, but for a stratocumulus field instead of a cumulus field.}
  \label{fig:stcu}
\end{figure}

\ci{}{Note that a single cloud field was used in this study, hence the
sensitivity to these conclusions to cloud characteristics such as cloud cover,
liquid water path, cloud base height or cloud geometrical thickness was not
investigated.  From the literature and the few sensitivity tests that were
performed, it can be assumed that the amplitude of 3D effects increases with
cloud geometrical thickness and total cloud cover up to medium values (e.g.
\v{C}rnivec and Mayer (2019); Gristey et al. (2020)). In multi-layer
cloud fields, entrapment between cloudy layers is often the dominant 3D effect
(Hogan et al., 2019) hence the relative importance of surface albedo might be
less. In overcast cloud fields, 3D effects are generally less intense due to
the lack of cloud sides for radiation to interact with, which might modify the
shape of the functional in addition to its amplitude.}

\qu{9. Ln 313. Please elaborate on ``extremely fast post-treatment", what do you
mean exactly?}

\re{We meant evaluating a polynomial function, this was added to clarify our
point.}

\ci{}{By means of an extremely fast post-treatment (evaluating a polynomial
function), estimates of surface and TOA fluxes, as well as their uncertainties,
were estimated for 200 values of surface albedo regularly spaced between 0 and
1.}

\qu{10. Ln 362. What are operational radiative transfer models, do you have a
reference?}

\re{We do not have a specific reference in the literature, here ``operational
radiative transfer models" refers to the RT models used in operational
atmospheric models such as those used for NWP, climate modeling or remote
sensing.}

\qu{11. Ln 367. should this be a new paragraph?}

\re{Yes thank you.}

\qu{12. Ln 368. Up to here I thought of the An coefficients as domain-mean
coefficients for domain-mean fluxes. How does this work: you determine An for
each grid cel land the An reported in earlier figures in the horizontal mean of
that?}

\re{You are right that this was confusing! The An coefficients until that point
were for the domain-mean fluxes; in Fig. 7, we performed additional simulations
with another code dedicated to estimating flux maps and images, and hence the
An there refered to horizontally resolved quantities. We added the following
sentence to clarify:}

\ci{}{These coefficients were obtained from additional simulations performed
with a slightly different 3D MC model, optimized for spatially discretized
radiative estimates such as flux maps and physically-based rendering
(\texttt{htrdr}, Villefranque et al., 2019).}

\qu{13. Ln 379. I suggest to start ``Figure 7 shows" as a new paragraph.}

\re{Done.}

\qu{14. Ln 435-437: Can you elaborate on the advantanges you expect from FMC
for operational Monte Carlo radiative transfer in atmospheric models? In
simulations with constantly evolving cloud fields, there is probably no use in
finding a polynomial function of radiative fluxes as function of albedo,
because the main interest is in radiative fluxes at each time step.}

\re{Indeed, the idea would be to derive new formulations based on the FMC
method, to output functionals of other parameters such as the 3D field of
extinction coefficients to accelerate the coupling between a MC code and a LES
for instance. It could also be used in retrieval algorithms. This application
was only an example of use of the FMC method in a non-operational context.
Other use might be much more challenging as discussed in Sec. 4.}

\qu{15. Appendix A. I do not quite follow the appendix after equation A4.}

\re{We are sorry that Appendix A was difficult to follow. We considered
rewriting it but one of the comments of the other reviewer was that this
appendix was very useful to understand the FMC method, and even suggested to
move some of it in the main text. It appears to us that the readibility and
usefulness of the formal derivation of the method is quite subjective and hence
we decided to keep the text in its original version.}

\subsection*{Grammar and language}~\vspace{-1.5\baselineskip}

\qu{16. Ln 100. ``cofficient" $\rightarrow$ ``coeficient"}

\qu{17. Ln 102-103. perhaps rewrite to `` phase function asymmetry parameter,
cloud top height, solar angles, and viewing geometry.}

\qu{18. Ln 169. ``albedo" $\rightarrow$ ``the albedo"}

\qu{19. Ln 320. ``High sun angles" is ambiguous, better use either ``high zenith
angle" or  ``low elevation angle".}

\qu{20. Ln 325. ``are afforbed better oppurtunity to be reflected", perhaps
rephrase to ``is more likely reflected" }

\qu{21. Ln 346. ``depend" $\rightarrow$ ``depends"}

\re{All done, thank you!}

\newpage
\section*{Response to Reviewer 2}

Thank you for your work, comments and insights. Your enthusiasm was deeply
appreciated, as well as your suggestions to improve the readibility of the text
and the figure presentation. We hope you will be satisfied with our responses
to your review even if we did not include all your suggested modifications to
the manuscript.  In the following, your overview and specific comments are
reported in \textcolor{monbleu}{blue}, our answers are in black italics and
citations from the text in \textcolor{monvert}{green}.  Line numbers correspond
to the new manuscript.

\qu{Summary: Villefranque et al. present a relatively novel methodology they
call ``Functionalized Monte Carlo" (FMC) to estimate functional forms, instead
of scalar quantities, from 3D radiative transfer modeling. They use FMC to
estimate TOA and surface fluxes of solar radiation as a function of surface
albedo based on a single simulation with fixed surface albedo. Because of the
nature of the problem, surface albedo, which appears explicitly in the
radiative transfer equation, can be easily extracted and coefficients of a
polynomial can be learned. With the FMC approach they are able to estimate the
3D radiative effects of clouds on surface and TOA fluxes from a single
simulation, which makes this approach relatively computational efficient. In
addition, and the aspect the authors choose to emphasize in the manuscript, is
that the FMC approach can yield new insights into the 3D effect by decomposing
the 3D effect by the number of successive scatterings, which is represented by
the coefficients of the polynomial. In particular, they show that surface
albedo strongly modulates the 3D effect of shallow cumulus clouds through the
entrapment process.}

\qu{Recommendation: Overall, I found this paper was very interesting,
well-written, and makes an important contribution to the existing literature. I
would recommend it for publication. Below are some minor comments for
improvement of the manuscript.}

\subsection*{General comments:}~\vspace{-1.5\baselineskip}

\qu{-	I found Appendix A and the discussion at the end to be quite helpful in
understanding the FMC method. Can some of this be moved up into the methodology
section?}

\re{We are glad that this part was useful for the comprehension of the FMC
method. However, one of the comments of the other reviewer was that Appendix A
was difficult to follow after Eq. 4. It appears to us that the readibility and
usefulness of the formal derivation of the method is quite subjective and hence
we decided to keep the text in its original version.}

\qu{-	Also, could you include some discussion about how easy/difficult it would
be to employ FMC to learn functions of other input quantities e.g. solar zenith
angle or mean droplet effective radius? It is unclear to me after reading this
how useful this method is for general problems vs. just this specific problem.}

\re{This discussion was already held in Sec. 5 to some extent (see L. 422 of
the original manuscript). Indeed, this particular example was particularly
straightforward and well-suited for using the FMC method. It is more difficult
with solar zenith angle because it is not a direct parameter in the Radiative
Transfer Equation, and with the field of effective radius because of the huge
amount of data. We modified part of this paragraph to clarify this point.}

\subsection*{Specific comments:}~

\qu{-	L56: they have been used often on small domains, but not yet in global or
operational models}

\re{Indeed. The other reviewer also suggested that we mention recent efforts to
couple a 3D MC radiative transfer code in a Large-Eddy Simulation, hence L. 56
was slightly modified:}

\ci{}{They have, however, been used often to study complex interactions between
3D clouds and radiation --- including recently in a coupled high-resolution
simulation, see Veerman et al. (2022) --- and consequently to evaluate and
improve fast approximate RT models}

\qu{-	Fig 2: It would be helpful to put the $\hat{\alpha}$ information for the
left panels in a legend. The grey line/shading is really only noticeable in the
SFC case for thick clouds, so I had completely overlooked it when looking at
the TOA plot on top.}

\re{Done.}

\qu{-	Fig 3b: Why is the standard MC method slower than the FMC for
$\hat{\alpha}=0.4$?}

\re{This is because there is some randomness in the path computing times, as
sometimes the CPUs might be used by the system during the simulations. We redid
the simulations for the slabs and reproduced Fig3b, here shown in
Fig.~\ref{fig:times}b. You can see that the general behaviour and results are
the same as Fig 3b of the manuscript (here reproduced in Fig.~\ref{fig:times}a)
but in this new set of simulations, the computing times for $\hat\alpha=0.4$ in
the thick case are the same in the standard and functionalized MC simulations.}
\begin{figure}\centering
  \includegraphics[width=.5\textwidth]{figure_time.pdf}~
  \includegraphics[width=.5\textwidth]{figure_time_slabs_print0_debug.pdf}
  \caption{Left: Fig 3b from the manuscript. Right: same but with a different
  set of simulations, run for 50k paths instead of 300k paths, with a more
  recent version of the code and on a different laptop.}
  \label{fig:times}
\end{figure}

\qu{-	Fig 4b: I think showing the std of liquid water as shading on the blue
curve might be more intuitive than showing the FSD as a separate red curve.}

\re{Good idea, Fig. 4b and its caption were modified accordingly.}

\qu{-	L294: Remove ``:" and split into two sentences.}

\re{OK.}

\qu{-	L297: There is no in-text reference to Fig. 4 throughout the manuscript.}

\re{Thank you, a reference to Fig. 4 was added L\TBD.}

\qu{-	L297: No need to spell out the time of day, what's in parentheses is just
fine.}

\re{OK.}

\qu{-	L300: Remove extra space ``26\%"}

\re{OK.}

\qu{-	Fig 5: In the text the language used to describe the results shown in
this figure talks about how the 3D effects vary across SZA for different
surface albedos. To me, it would be easier to read this figure alongside the
text if the plot matched this viewpoint and showed SZA on the x-axis and
different surface albedos as different lines. Alternatively, because albedo is
parameter in the function being learned, i.e. the continuous variable, and SZA
is discrete, it might be better to update the text to match this description.
Or, a third approach would be to try a contour plot of 3D effects with SZA on
y-axis and surface albedo still on the x-axis.}

\re{We appreciate the cognitive difficulty raised by the difference in
presentation in the text and figures. However we decided not to change the
x-axis and colors to keep the emphasis on the fact that the albedo is indeed
the continuous variable. We tried the contour plots as suggested (see
Fig.~\ref{fig:contours} of this document) but were not entirely convinced that
it was any easier to read. However this is somehow subjective, we decided to
submit the contour plot figures in a Supplemental Information, which we refer
to in the caption of Fig. 5 as an alternative visualization of the same data.}

\begin{figure}\centering
  \includegraphics[width=.5\textwidth]{fig_xalb_ysza_cont3Deffsurf_csza_ARMCu.pdf}~
  \includegraphics[width=.5\textwidth]{fig_xalb_ysza_cont3Defftoa_csza_ARMCu.pdf}~\\
  \caption{Same as Fig. 5 of the manuscript but in the form of contour plots.}
  \label{fig:contours}
\end{figure}

\qu{-	L303: Can you make some statement about how this somewhat major
assumption impacts the results?}

\re{We think that the impact will be minor on the shape of the functional, but
that the amplitude of 3D effects might decrease when variations of effective
radii are accounted for. This, however, remains to be verified in subsequent
studies. We added the following comment to address this question:}

\ci{}{Assuming that droplets sizes tend to increase going from edges of clouds
to their cores, accounting for variations in effective radius would tend to
suppress variations in optical depth, which would shift 3D RT results slightly
towards 1D results. Although this remains to be verified, it is assumed that
the shape of the functional would only marginally be affected, while the
amplitude of 3D effects would be slightly lower.}

\qu{-	L342/344/377: ``zeroth" with no dash}

\re{OK, thank you.}

\qu{-	Fig 6a/b: It might be helpful to annotate this figure to highlight which
order the entrapment mechanism is most apparent.}

\re{Good idea, we added text in the figures of the upper panel (3D effects), to
highlight reflection orders where we see either 1D or 3D entrapped or escape.}

\qu{-	L350: ``change" should be ``chance"}

\re{Yes, thank you!}

\qu{-	L399-401: This sentence was very helpful for understanding FMC. Can this
idea be included in the first explanation of FMC in the methods section?}

\re{This sentence remains in the Section 4 but a similar sentence was added in
the methodology section.}

\ci{}{In the FMC framework, interpreting the total flux as the sum of $n$-times
reflected fluxes as per Equation (2) leads to estimating $\mathcal{A}$ as}

\qu{-	L403: ``synthesize"?}

\re{Yes thank you.}

\qu{-	L406: change ``insist" to ``focus" or ``emphasize"}

\re{OK.}

\qu{-	Table A1: Left align the algorithm column please}

\re{Yes this is much more readable thank you!}

\end{document}
