\begin{eqnarray}
  \mathcal{A}(\alpha ) &=& R_0 + T_{0}\alpha t_{1} + T_{0} \alpha r_1 \alpha
  t_2 + T_0 \alpha r_1 \alpha r_2 \alpha t_3 +\cdots \\
  &=& R_0+\left( T_{0}t_{1}\right) \alpha +\left(T_{0}r_{1}t_{2}\right) \alpha
  ^{2}+\left( T_{0}r_{1}r_{2}t_{3}\right) \alpha ^{3} +\cdots,
\label{eq:series}
\end{eqnarray}
%
In the linear category, the paths are not affected by
the parameter, hence the idea of reusing simulated paths to propagate a
different information (different source value) is straight-forward.
In the non-linear category however, a change in parameter might affect the
geometry of the photon paths, which raises a particular challenge: In this case, the
photon paths sampled during the MC simulations can be used to propagate a new
value of the input parameters in a very straightforward manner.

Acceleration of path-tracking MC methods is an active field of research that
goes beyond atmospheric sciences. In computer graphics, sophisticated ray
tracing techniques have been developed to solve the rendering equation
(radiative transfer in transparent media) \cite{burley_design_2018} in highly
complex scenes \cite{wald_embree_2004}. They were extended to cloudy
atmospheres and other heterogeneous scattering media
\cite{villefranque_path-tracing_2019, sans_null-collision_2021}, resulting in
MC computations that no longer increase with the amount of details in a scene
(e.g., 3D cloud fields, solar plants, and entire cities). Of course the cost of
the simulation still depends on the amount of scattering and surface
reflections that characterizes the path sample (tracking paths in deep clouds
takes longer than in clear sky).

A perhaps more traditional way to accelerate MC simulations is to introduce
sampling strategies that either reduce noise (variance reduction techniques) so
that fewer samples are needed to estimate a quantity to a given accuracy, or
yield shorter paths (e.g. with the Russian roulette techniques) hence resulting
in shorter computation times for a fixed number of paths
\cite{iwabuchi_efficient_2006, buras_efficient_2011, novak_residual_2014,
kutz_spectral_2017}. Another common way to speed up the computation when
multiple quantities must be estimated is to make more intensive use of the
sampled paths, for instance by concurrently computing contributions to multiple
sensors at each scattering and reflection events as per the local estimate
method \cite{marchuk_elements_1980}, or by estimating the contribution of a
single path to multiple wavelengths at once as per the ALIS
\cite{emde_alis_2011} or hero wavelength \cite{wilkie_hero_2014} methods.

The strong computational constraints under which these \emph{operational} RT
models are designed have motivated the use of simplifying assumptions. The most
common one is the dimensional reduction of the radiative transfer equation
(RTE) from five (three spatial plus two directional) to two by assuming
horizontal and azimutal invariance. This allows to derive a flux form of the
RTE by further integrating over up and down hemispheres, eventually yielding
so-called two-stream models. Once this paradigm --- often refered to as
one-dimensional (1D) --- is set, it becomes very difficult to account for the
fact that most cloud fields are spatially heterogeneous and organized; the
neglect of which have been proven to induce large biases in radiative radiances
and fluxes estimates across a wide range of scales \cite{marshak_3d_2005}.
Quantifying the effect of these biases on retrievals and on simulated weather
and climate, as well as conceiving efficient RT models that mitigate these
biases, is an active research field of atmospheric sciences.

Most atmospheric models suffer from
overly simple assumptions in the treatment of radiation in the presence of
clouds; especially for highly heterogeneous and geometrically complex clouds.
Indeed, the radiative transfer (RT) models that are used for data assimilation
\cite{saunders_update_2018}, weather forecasting \cite{hogan_flexible_2018} and
climate modeling \cite{fouquart_computations_1980, pincus_fast_2003,
barker_assessing_2003} are efficient but tainted with systematic biases that
may affect the retrievals of cloud and surface properties from remote
observations \cite{iwabuchi_multi_2003} and retroact on weather forecasts
and climate change projections. Unbiased numerical methods such as the Monte
Carlo (MC) methods have most often been deemed too computationally expensive to
be used online in atmospheric models, and hence have remained confined to
offline evaluations of approximate RT schemes using high-resolution cloud model
data \cite{pincus_computational_2009}.


Let us illustrate these steps in the case of the broadband,
horizontally-averaged reflected flux at $\toa$ (the quantity of interest
$\overline{F}(z_\toa)$), keeping the surface albedo $\alpha$ as a variable. The
classical MC algorithm to compute $F$, the contribution to
$\overline{F}(z_\toa)$ of the paths that start at $\x_0=(x_0,y_0,z_\toa)$ in
the sun direction $\w_0$ is:

\begin{align}
  F =&\int\dg_0 \pg(\gamma_0 | \x_0,\w_0)\Bigg\{ H(\gamma_0 \in \toa)\{F_\odot\} +
               (1-H(\gamma_0 \in \toa)) \bigg\{ \alpha \int_{2\pi}\dw_1\pw(\w_1|\w_0')  \times \nonumber\\
               &\int\dg_1\pg(\gamma_1 | \x_1,\w_1)\bigg\{ H(\gamma_1 \in \toa)\{F_\odot\} +
                (1-H(\gamma_1 \in \toa)) \Big\{ \alpha \int_{2\pi}\dw_2\pw(\w_2|\w_1')  \times \nonumber\\
                &\int\dg_2\pg(\gamma_2 | \x_2,\w_2)\Big\{ H(\gamma_2 \in \toa)\{F_\odot\} +
                 (1-H(\gamma_2 \in \toa)) \big\{ \alpha \int_{2\pi}\dw_3\pw(\w_3|\w_2') \times \nonumber\\
                  &... \big\}\Big\}\Big\}\bigg\}\bigg\}\Bigg\} \nonumber
\label{eq:first}
\end{align}

$F_\odot$ is the incoming solar radiation at $\toa$. $\pw(\w_j| \w_i)$ is the
normalized ground reflectivity function that gives the probability of being
reflected into $\w_j$ from the incoming direction $\w_i$.  $\Gamma$ is a random
variable ``multiple-scattering path''. $p_\Gamma(\gamma | \x, \w)$ is the
density probability function of a path $\gamma$ that starts at $\x$ in
direction $\w$ and reaches either the ground or the $\toa$ with a direction
$\w'$. It is a condensate form representing multiple scattering and absorption
in a heterogeneous medium. See Appendix~\ref{app:pgamma} for a complete
expression.  $H(\gamma \in \toa) = 1$ if $\gamma$ reaches $\toa$ and $0$
otherwise (which means that $\gamma$ has reached the surface instead). Upon
reaching the ground, the photon gets reflected with probability $\alpha$ (the
ground albedo) while upon reaching $\toa$, the path ends and the photon weight
($F_\odot$) is added to the flux counter. Other paths (absorbed in the medium
or at the ground) do not contribute to the reflected flux at $\toa$ hence their
weights are zero and they do not need to appear explicitely in the formulation.

The expression \eqref{eq:first} can be factorized into:
\begin{align}
  F =&\sum_{n=0}^\infty F_n
\end{align}

where $F_n$ is the contribution of the paths that have lived $n \geq 0$
reflections at the surface to the total reflected flux:

\begin{align}
  F_0 &= \int\dg_0 \pg(\gamma_0 | \x_0,\w_0)H(\gamma_0 \in \toa)\{F_\odot\} \\
  \forall n > 0, \quad F_n &= \int\dg_n \pg(\gamma_n | \x_n,\w_n)H(\gamma_n \in \toa)\{F_\odot\}\times \nonumber \\
      &\quad \prod_{i=0}^{n-1}\int\dg_i \pg(\gamma_i | \x_i,\w_i) (1-H(\gamma_i \in \toa)) \alpha \int_{2\pi}\dw_i\pw(\w_{i+1}|\w_i') 
\end{align}

Each term of the sum is the contribution of the paths that have lived a given
number of reflections at the surface: the first term ($n=0$) is the
contribution of the paths that have been reflected by scattering in the volume,
the other terms ($n>0$) are the contributions of the paths that have been
reflected $n$ times at the surface. A path of order $n$ which has lived $n \geq
1$ reflections at the surface is made of one sub-path that has started at
$\toa$ and ended at the surface, of $n-1$ sub-paths that have started at the
surface and went back to the surface, and of one sub-path that has started at
the surface and ended at the $\toa$. During a MC simulation, one can always
store the weights of the paths that reach $\toa$ in different counters,
depending on the number of surface reflections the path has lived before being
reflected to $\toa$. At the end, the total reflected flux at $\toa$ is the sum
of the different MC estimates $\check F_n = \mathbb{E}[F_n]$. $\forall n >0,
F_n$ is actually a function of $\alpha$, so from now on we will write
$F_n(\alpha)$.

Now let us use importance sampling to isolate $\alpha$ from the rest of the
expression. All the following expressions are for $n>0$:

\begin{align}
  F_n(\alpha) = \prod_{i=0}^{n-1}(\frac{\alpha}{\hat\alpha})&\int\dg_n \pg(\gamma_n | \x_n,\w_n)H(\gamma_n \in \toa)\{F_\odot\} \times \nonumber\\
   & \prod_{i=0}^{n-1}\int\dg_i \pg(\gamma_i | \x_i,\w_i) (1-H(\gamma_i \in \toa)) \hat\alpha \int_{2\pi}\dw_i\pw(\w_{i+1}|\w_i') & \nonumber\\
\end{align}
and if $\alpha$ is independent of the incoming direction and uniform in space
and spectrum (which is not necessary but simplifies the expression here):
\begin{align}
  F_n(\alpha) = (\frac{\alpha}{\hat\alpha})^n&\int\dg_n \pg(\gamma_n | \x_n,\w_n)H(\gamma_n \in \toa)\{F_\odot\}\times \nonumber\\
                         & \prod_{i=0}^{n-1}\int\dg_i \pg(\gamma_i | \x_i,\w_i) (1-H(\gamma_i \in \toa)) \hat\alpha \int_{2\pi}\dw_i\pw(\w_{i+1}|\w_i')\nonumber\\
\end{align}
in which the second term (following $(\alpha/\hat\alpha)^n$) is simply
$F_n(\hat\alpha)$ which means that
\begin{align}
  F_n(\alpha) = (\frac{\alpha}{\hat\alpha})^nF_n(\hat\alpha)
\end{align}
and so
\begin{align}
  F(\alpha) =& F_0 + \sum_{n=1}^\infty (\frac{\alpha}{\hat\alpha})^n F_n(\x_0,\hat\alpha)
  \label{eq:func}
\end{align}

Now we recognize that the terms $F_n$ are exactly the same quantities as were
estimated by the classical MC algorithm before, only with a surface of albedo
$\hat\alpha$ instead of $\alpha$. Which means that using only MC simulation
with $\hat\alpha$ (not necessarily 1), we are able to estimate $F(\alpha)$ for
any value of $\alpha$. 
