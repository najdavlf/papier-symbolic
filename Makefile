SHELL := /bin/bash

#----------------------------- COMPILER SANS LE MAKEFILE -----------------------------#
# TEXINPUTS=tex/:figures/:$TEXINPUTS pdflatex -output-directory=build si_symbolic.tex #
#-------------------------------------------------------------------------------------#

# will create $(PROJ).pdf in BUILD_DIR
PROJ=symbolic

BUILD_DIR=build
TEX_DIR=tex
BIB_DIR=bib

VOLD=_v2.2
VNEW=_v2.3

PDFLATEX_OPTS += -output-directory=$(BUILD_DIR)
PDFLATEX = TEXINPUTS=$(shell pwd)/$(TEX_DIR):$(shell pwd)/figures/$(PROJ):$(TEXINPUTS) xelatex $(PDFLATEX_OPTS)
BIBTEX = BIBINPUTS=./:./$(BIB_DIR)/:../$(BIB_DIR)/:$(BIBINPUTS) bibtex

DOC=$(BUILD_DIR)/$(PROJ).pdf
REP=$(BUILD_DIR)/revisions.pdf
SI=$(BUILD_DIR)/si_$(PROJ).pdf
DIFF=$(BUILD_DIR)/diff$(VOLD)$(VNEW).pdf

all: $(DOC) $(REP) $(SI)
diff: $(DIFF)
.PHONY : all diff clean # not a file name

# only deps
$(TEX_DIR)/$(PROJ).tex:

$(BUILD_DIR)/%.blg: $(TEX_DIR)/%.tex $(BIB_DIR)/biblio.bib
	mkdir -p $(BUILD_DIR)
	$(PDFLATEX) $< && cd $(BUILD_DIR) && $(BIBTEX) `echo $< | sed -e "s/.*\/\(.*\)\.tex/\1/"`
	$(PDFLATEX) $<

$(DOC): $(TEX_DIR)/$(PROJ).tex $(BUILD_DIR)/$(PROJ).blg
	mkdir -p $(BUILD_DIR)
	$(PDFLATEX) $(PROJ).tex 

$(REP): $(TEX_DIR)/revisions.tex
	mkdir -p $(BUILD_DIR)
	$(PDFLATEX) revisions.tex 

$(SI): $(TEX_DIR)/si_$(PROJ).tex
	mkdir -p $(BUILD_DIR)
	$(PDFLATEX) si_$(PROJ).tex 

### DIFF files using latexdiff

$(TEX_DIR)/diff$(VOLD)$(VNEW).tex: $(TEX_DIR)/$(PROJ)$(VOLD).tex $(TEX_DIR)/$(PROJ)$(VNEW).tex
	latexdiff $(TEX_DIR)/$(PROJ)$(VOLD).tex $(TEX_DIR)/$(PROJ)$(VNEW).tex > $(TEX_DIR)/diff$(VOLD)$(VNEW).tex

$(DIFF): $(TEX_DIR)/diff$(VOLD)$(VNEW).tex $(BUILD_DIR)/diff$(VOLD)$(VNEW).blg
	$(PDFLATEX) $(TEX_DIR)/diff$(VOLD)$(VNEW).tex && $(PDFLATEX) $(TEX_DIR)/diff$(VOLD)$(VNEW).tex

clean:
	if [ -d build ]; then rm -fr build; fi
